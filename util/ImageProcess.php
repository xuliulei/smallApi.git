<?php
/**
 * 图片处理类:包含压缩，裁剪策略
 * @author:jiangpengfei
 * @date:2017-11-13
 */
//use Util\FileUtil;

namespace Util;

class ImageProcess{
    const THRESHOLD = 1280;
    const COMPRESSION_QUALITY = 95;
    /**
     * 将图片处理至二进制流
     * @param $filePath 文件路径
     * @return mix 文件的二进制流，或false代表文件类型不支持
     */
    public static function processImageToBlobStrategy($filePath,$needCompression = false,$needCrop = false,$cropWidth,$cropHeight,$cropX,$cropY){

        $type = FileUtil::getFileType($filePath);
        $extName = null;    //后缀名
        switch($type){
            case FileUtil::FILE_TYPE_JPG:
                $extName = "jpg";
                break;

            case FileUtil::FILE_TYPE_GIF:
                $extName = "gif";
                break;

            case FileUtil::FILE_TYPE_PNG:
                $extName = "png";
                break;

            default:
                return false;
        }

        $imagick = new \Imagick($filePath);;

        $width = $imagick->getImageWidth();
        $height = $imagick->getImageHeight();

        //检查是否需要裁剪
        if($needCrop){
            $imagick->cropImage($cropWidth,$cropHeight,$cropX,$cropY);
            $width = $cropWidth;
            $height = $cropHeight;
        }

        //检查是否需要压缩
        if($needCompression){
            
            $thumbRate = 1;     //缩放率
            if($width > self::THRESHOLD && $height > self::THRESHOLD){
                //都大于1280
                $max = 0;
                $min = 0;

                if($width > $height){
                    $max = $width;
                    $min = $height;
                }else{
                    $min = $width;
                    $max = $height;
                }

                $rate = $max / $min;
                if($rate > 2){
                    //取最小的缩放为1280
                    $thumbRate = $min / self::THRESHOLD;
                }else{
                    //取最大的缩放为1280
                    $thumbRate = $max / self::THRESHOLD;
                }

            }else if($width > self::THRESHOLD || $height > self::THRESHOLD){
                //其中之一大于1280
                $max = 0;
                $min = 0;

                if($width > $height){
                    $max = $width;
                    $min = $height;
                }else{
                    $min = $width;
                    $max = $height;
                }

                $rate = $max / $min;
                if($rate < 2){
                    //如果宽高比小于2，max压缩至1280
                    $thumbRate = $max / self::THRESHOLD;
                }
            }

            if($thumbRate != 1){    
                $width = ceil($width/$thumbRate);
                $height = ceil($height/$thumbRate);
                $imagick->thumbnailImage($width,$height);
            }

            $imagick->setImageFormat("jpeg");
            $imagick->setImageCompression(\imagick::COMPRESSION_JPEG);   //设置成jpeg，有损压缩
            $extName = "jpg";      //更改格式后缀
            $imagick->setImageCompressionQuality(self::COMPRESSION_QUALITY);      //压缩
        }
        
        $imageBlob = $imagick->getImageBlob();

        $imageInfo = array();
        $imageInfo['width'] = $width;
        $imageInfo['height'] = $height;
        $imageInfo['blob'] = $imageBlob;
        $imageInfo['extName'] = $extName;

        return $imageInfo;
    }

};