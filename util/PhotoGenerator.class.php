<?php
 /**
  * 照片生成类
  * author:jiangpengfei
  * date:2017-04-2
  */
namespace  Util;

class PhotoGenerator{
    public static $familyPhotoResource = [
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIDKAVXKmAACCQrzaxdw195.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIGiAcC0aAABzAZZEmII488.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIJeAL8eWAABn4qH1djY697.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIKqAXa7fAACs6ukW49U410.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIMCABqZfAACALIbo__s039.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmINWAYeU9AACYq8VNsZU967.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIPKARRmRAACMdMf2pek163.jpg"
    ];

    public static $userPhotoResource = [
        "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmH_-AIYTLAAAHLtDFCbg683.jpg",
    ];

    public static $personPhotoResource = [
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIDKAVXKmAACCQrzaxdw195.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIGiAcC0aAABzAZZEmII488.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIJeAL8eWAABn4qH1djY697.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIKqAXa7fAACs6ukW49U410.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIMCABqZfAACALIbo__s039.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmINWAYeU9AACYq8VNsZU967.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIPKARRmRAACMdMf2pek163.jpg"
    ];

    public static $activityPhotoResource = [
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIDKAVXKmAACCQrzaxdw195.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIGiAcC0aAABzAZZEmII488.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIJeAL8eWAABn4qH1djY697.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIKqAXa7fAACs6ukW49U410.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIMCABqZfAACALIbo__s039.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmINWAYeU9AACYq8VNsZU967.jpg",
      "http://img1.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIPKARRmRAACMdMf2pek163.jpg"
    ];

    /**
     * 从资源中随机选择一张家族照片作为头像
     */
    public static function generateFamilyPhotoFromResource(){
        $n = rand(0,count(self::$familyPhotoResource) - 1);
        return self::$familyPhotoResource[$n];
    } 

    /**
     *  从资源中随机选择一张人物照片作为头像
     */
    public static function generatePersonPhotoFromResource(){
        $n = rand(0,count(self::$personPhotoResource) - 1);
        return self::$personPhotoResource[$n];
    } 

    /**
     * 从资源中随机选择一张照片作为活动配图
     */
    public static function generateActivityPhotoFromResource(){
        $n = rand(0,count(self::$activityPhotoResource) - 1);
        return self::$activityPhotoResource[$n];
    }

    /**
     * 从资源中随机选择一张照片作为用户头像
     */
    public static function generateUserPhotoFromResource(){
        $n = rand(0,count(self::$userPhotoResource) - 1);
        return self::$userPhotoResource[$n];
    }

    /**
     * 随机生成家族照片
     *
     *
     */
    public static function generateFamilyPhoto($familyName){

    }

    /**
     * 随机生成人物照片
     *
     *
     */
    public static function generatePersonPhoto($personName){

    }
 };