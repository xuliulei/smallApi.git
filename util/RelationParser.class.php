<?php

namespace Util;

    class RelationParser{
        private $fathers;		//父亲数组
        private $mothers;		//母亲数组
        private $brothers;		//兄弟数组
        private $sisters;		//姐妹数组
        private $spouses;		//配偶数组
        private $sons;			//儿子数组
        private $daughters;		//女儿数组
        private $person;
        /**
         * 构造函数
         * $person是Person对象，可通过father,mother,brother,sister,spouse,son,daughter取得特定格式的id字符串
         * id字符串的格式类似于  1,2,3,4,5,
         */
        public function __construct($person){
            $this->person = $person;
            $this->parser();			//调用解析器解析出这个结构体
        }
        
        private function parser(){
            $this->fathers = $this->idSplit($this->person->father);
            $this->mothers = $this->idSplit($this->person->mother);
            $this->brothers = $this->idSplit($this->person->brother);
            $this->sisters = $this->idSplit($this->person->sister);
            $this->spouses = $this->idSplit($this->person->spouse);
            $this->sons = $this->idSplit($this->person->son);
            $this->daughters = $this->idSplit($this->person->daughter);
        }
        
        private function idSplit($str){
            if($str != ""&&$str != NULL)
                return explode("|", substr($str,0,strlen($str)-1));		//返回一个id数组
            else
                return array();											//返回一个空数组
        }
        
        public function getFathers(){
            return $this->fathers;
        }
        
        public function getMothers(){
            return $this->mothers;
        }
        
        public function getBrothers(){
            return $this->brothers;
        }
        
        public function getSisters(){
            return $this->sisters;
        }
        
        public function getSpouses(){
            return $this->spouses;
        }
        
        public function getSons(){
            return $this->sons;
        }
        
        public function getDaughters(){
            return $this->daughters;
        }
    };
?>