<?php

namespace Util;

class Util{

    //生成随机码
    public static function generateRandomCode($len){
        $code="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
        $str="";
        for($i=0;$i<$len;$i++){
            $str=$str.substr($code,rand(0,strlen($code) - 1),1);;
        }
        return $str;
    }

    /**
    * 获取当前时间
    */
    public static function getCurrentTime(){
        return date("Y-m-d H:i:s");
    }

    /**
    * 获取当前日期
    */
    public static function getCurrentDate(){
        return date("Y-m-d");
    }

    /**
    * 获取请求ip
    */
    
    public static function getRequestIp(){
        return $_SERVER["REMOTE_ADDR"];
    }

    /**
    * 输出结果的函数
    * @param success 是否成功 true是成功，false是失败
    */
    public static function printResult($errorCode,$data){
        $result['errorCode']=$errorCode;
        if(is_array($data)){
            $result['msg'] = "";
            $result['data'] = $data;
        }else{
            $result['msg'] = $data;
            $result['data'] = new \stdClass();
        }
        echo json_encode($result);
    }

    /**
    * 格式化日期,yy-mm-dd hh:ii:ss 转换成yy-mm-dd
    */
    public static function dateFormat($date){
        if($date == '' || $date == null)
            return '';
        $date = date_create($date);
        $newDate = date_format($date, "Y-m-d");
        return $newDate;
    }

    /**
     * 日期比较
     * @param $date1 日期一
     * @param $date2 日期二
     * @return 负数代表日期一早于日期二，0代表日期一等于日期二，正数代表日期一迟于日期二
     */
    public static function dateCompare($date1,$date2){
        $unixDateTime1 = strtotime($date1);
        $unixDateTime2 = strtotime($date2);
        return $unixDateTime1 - $unixDateTime2;
    }

    /**
     * 获取当前时间
     */
    public static function getMicroTime(){
        list($usec,$sec) = explode(" ",microtime());
        return ((float)$usec + (float)$sec);
    }
        
    public static function exceptionFormat($e){
        $line = $e->getLine();
        $file = $e->getFile();
        $message = $e->getMessage();
        $trace = json_encode($e->getTrace());
        return "$file($line):$message\n$trace\n";
    }

    public static function generateRegisterEmail($auth){
        return "欢迎注册爱族群,这里是激活链接，http://{$_SERVER['HTTP_HOST']}" .$GLOBALS['ROUTER_TYPE']."/auth/{$auth}，12小时内有效";
    }

    public static function generateEmailBindingEmail($auth){
        return "绑定邮箱点击下面链接，http://{$_SERVER['HTTP_HOST']}" .$GLOBALS['ROUTER_TYPE']."/authemailbind/{$auth}，30分钟内有效";
    }



    /**
     * 生成验证码
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateVcode($num){
        $res = "";
        while( ($num--) > 0 ){
            $res .= rand(0,9);
        }
        return $res;
    }

    /**
     * 生成短信验证序号
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateOrder($num){
        $res = "";
        while( ($num--) > 0 ){
            $res .= rand(0,9);
        }
        return $res;
    }

    /**
     * 生成二维码
     * @param $string 二维码存储的信息
     * @return mix 二维码的存储地址string,-1代表网络错误，-2代表上传失败
     */
    public static function generateQRCode($string){
        $tmpName = Util::generateRandomCode(18);//18为随机数字
        $path = "/tmp/$tmpName.png";

       //$path="d:/tmp/$tmpName.png";
        //二维码的缓存地址
        //第一个参数是二维码的内容，
        //第二个参数是文件路径，
        //第三个参数可以是'L',M','Q','H',ECC level，应该是二维码的容错率
        //第三个参数是点的大小,
        //第五个参数是边框的大小,
         \PHPQRCode\QRcode::png($string,$path,'H', 4, 2);
        //添加公司logo位于二维码中央
        $QR = $path;
        $logo = "assets/logo.png"; //放logo图片的地址
        if ($logo !== FALSE) {
            $QR = imagecreatefromstring(file_get_contents($QR));
            $logo = imagecreatefromstring(file_get_contents($logo));
            $QR_width = imagesx($QR);//二维码图片宽度
            $QR_height = imagesy($QR);//二维码图片高度
            $logo_width = imagesx($logo);//logo图片宽度
            $logo_height = imagesy($logo);//logo图片高度
            $logo_qr_width = $QR_width / 3.5; //3.5 控制logo与二维码的比例
            $scale = $logo_width/$logo_qr_width;
            $logo_qr_height = $logo_height/$scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            //重新组合图片并调整大小
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }
        imagepng($QR,$path);
        imagedestroy($QR);

        $qrModule = $GLOBALS['QR_MODULE'];
        //使用curl传到远程服务器
        $data = array(
            'id_token'=>$GLOBALS['userId'].'|'.$GLOBALS['token'].'|'.$GLOBALS['usingDevice'],
            'file'=>new \CURLFile($path),
            'action'=>'file_action',
            'sub_action' => 'addFile',
            'module' => $qrModule,
            'deviceCode' => $GLOBALS['deviceCode']
        );

        //上传到图片服务器
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$GLOBALS['IMGSERVER']);
        curl_setopt($ch,CURLOPT_POST,true); //post方式上传
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); //成功返回true，输出内容
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data); //提交的数据
        $curl_result = curl_exec($ch); //执行提交
        curl_close($ch);//关闭
        if($res = json_decode($curl_result,true)){
            if($res['errorCode'] == 0){
                //获取到上传的地址
                $location = $res['data']['location'];
                //删除临时缓存文件
                if (is_file($path)) {
                    unlink($path);
                }
                return $location;
            }else{
                return $res['errorCode'];       //返回错误码
            }
        }else{
            return -1;
        }
    }

}